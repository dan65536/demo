# demo

---

###Summary

Class object having a metaclass of abc.ABCMeta must have all abstractmethods
overriden to be instantiated.  So they can be used as interfaces, in the Strategy
pattern.


## strategy_demo

   - currently runs  under Python3 only
   - use abstract metaclasses abc.ABCMeta to enforce strategy categories
   - access methods via
      - `container.strategy__method()`
      - or possibly , in future: `container.get(strategy).method()`

## strategy_filter

   - goal to run under PYthon 2.x and 3.x
   - organized for better understandability ( such is always the hope )
   - strategies can be initialized at class or instance level
      1 filter by tag
      2 optionally disable specific class(es) or instance(s) from inclusion
      