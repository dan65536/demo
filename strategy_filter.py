#/usr/bin/env python

from __future__ import print_function
import abc
import sys
import pprint

class AbstractAnimal (object):

    __metaclass__ = abc.ABCMeta

    Tag = 'Animal'

    @abc.abstractmethod
    def speak(self): return ''

    @abc.abstractmethod
    def walk(self): return ''

# ==
class Horse(AbstractAnimal) :

    def speak(self): return 'Neigh!'
    def walk (self): return '(trot)'

# ==
class  Duck(AbstractAnimal) :

    def speak(self): return 'Quack!'
    def walk (self): return '(waddle)'

#=====================================================

def allEqual( iterable ) :
    tup = tuple(iterable )
    return tup[:1] == () or not any(map(lambda el : el != tup[0] , tup[1:]))

def classOf(cls):
    return cls \
             if getattr(cls,'__class__',None) in (None,type,abc.ABCMeta) \
             else cls.__class__

#/////////////////////////////////////////////////////////////////////////////
not_met = lambda fn,x : not callable(fn) or not fn(x)
#/////////////////////////////////////////////////////////////////////////////

def StrategyInit ( collection, tag, disableFn = None ):

    try:
        i = iter(collection)
    except TypeError:
        i = iter((collection,))

    L = list(i)

    if not allEqual(map(lambda c:classOf(c).Tag, L)): return None
    return list(
             filter (lambda c:c.Tag == tag and not_met(disableFn,classOf(c)), L)
           )

#=====================================================

check_disable_attribute = lambda x: \
                          getattr(x,'disable',None)==True

#class myDuck(Duck): 
#  pass # ...
#  disable = True

#  -- or --:

class myDuck(Duck):  pass
myDuck.disable = True

class MyClass:
    a = StrategyInit ( [Horse,Duck,myDuck], 'Animal', check_disable_attribute )
    b = StrategyInit ( [ Horse(),Duck(),myDuck()], 'Animal' , check_disable_attribute )
    c = StrategyInit ( Horse, 'Animal', check_disable_attribute )
    d = StrategyInit ( myDuck, 'Animal', check_disable_attribute )
 
 
print(); pprint.pprint ( MyClass.a )
print(); pprint.pprint ( MyClass.b )
print () ; print ('\t==> ' + repr([ animal.walk() for animal in MyClass.b ]))
print(); pprint.pprint ( MyClass.c )
print(); pprint.pprint ( MyClass.d )

#=====================================================

class Container:

    def __init__(self,name,strategy_names=()):
        self.name = name
        self.strategy_names = strategy_names
        for tag in strategy_names:
            setattr(self,tag,[])

    def __repr__(self): return "Container("+self.name+")"

    def add_strategy (self, tag, obj_or_class):

        if tag in self.strategy_names \
         and obj_or_class.Tag.lower() == tag.lower():
            l = getattr(self,tag)
            l += [obj_or_class]     

print()

if True:
    c = Container("my Animals", ("animal",))
    c.add_strategy( "animal", myDuck() )
    c.add_strategy( "animal", Horse() )
    for x in c.animal:
        print("animal speech from {} : {}".format(c, x.speak()))
