#!/usr/bin/env python3

from __future__ import print_function
import abc

import sys


class Do_Something_Strategy_Type ( metaclass = abc.ABCMeta ):

   tag = 'do_something'

   @classmethod
   def register_all_strategies_by_tag ( cls ):
      for each_class in All_Strategies[cls.tag]:
          cls.register(each_class)


#============================================================

class BaseStrategy:

    def call_none(self,*x,**kw):
        raise NotImplementedError("*** CALL_NOT_ALLOWED ; not found")

    def call_not_allowed(self,*x,**kw):
        raise NotImplementedError ( "*** CALL_NOT_ALLOWED ; not callable" )

class Do_Something_impl_1 (BaseStrategy) :

    def __init__( self , tgt ) : self.target = tgt

    def anything( self , *x, **kw ) : 
      print ( 'thing() EVOKED ON ', '(' + self.target + ')\n\tWITH', x, '\n\tAND ', kw )

class Do_Something_impl_2 (BaseStrategy) :

    def __init__( self , tgt ) : self.target = tgt

    def anything( self , *x, **kw ) : 
      print ( 'thing() evoked on ', '(' + self.target + ')\n\twith', x, '\n\tand ', kw )

class Strategy_Referencing_Parent (BaseStrategy) :

    def __init__(self,parent_object=None):
        self.parent = parent_object

    def anything( self , *x, **kw ) : 
        print ("A different strategy , called with args : ",x,kw)
        return self.parent

#============================================================

class HighLevel:

    def __init__(self, sub_objs):

        self.sub_objs = sub_objs

    def set_obj_for_strategy(self, strategy, obj): 

        self.sub_objs[strategy] = obj

    def get_obj_for_strategy(self, strategy):

        subObj  = self.sub_objs.get(strategy)
        if subObj and issubclass(subObj.__class__, All_Strategies[strategy]):
            return subObj
        else:
            return None

    def __getattr__(self, strategy_plus_method):

        strategy, method_name = strategy_plus_method.split("__")

        subObj = self.sub_objs.get(strategy, None)

        if subObj is None:
            print( "*** CALL_NOT_ALLOWED b/c sub-object not found   ", file=sys.stderr )
            raise NotImplementedError

        elif issubclass(subObj.__class__, All_Strategies[strategy]):

            method = getattr( subObj, method_name, None)
            if callable(method):  return method
            else: 
                return subObj.call_not_allowed

        else:
            return subObj.call_none


#===   BEGIN     ===== MAIN function =====================

# ----- CATALOG of all available strategies and eligible implementations -----

All_Strategies = {  # -> a global which is accessed by the register...() strategy class method(s)

   'do_something' : ( Do_Something_impl_1 ,
                      Do_Something_impl_2 ,
                      Strategy_Referencing_Parent
                    ),

}



Do_Something_Strategy_Type . register_all_strategies_by_tag ()

print() # ========

hl_1 = HighLevel(
			{ 'do_something': Do_Something_impl_1("impl-1 Instance :: ") }
)

hl_1 . do_something__anything ( ' some arg ' , 'another arg ' , a="b", key="somevalue")

print() # ========

hl_2 = HighLevel(

			{ 'do_something': Do_Something_impl_2("impl-2 Instance :: ") }
)

hl_2 . do_something__anything ( 'some arg ' , 'another arg ' , a="b", key="somevalue")
 
print("\nunrecognized method: ") # ========

try:
  hl_2 . do_something__otherstuff ( 'Some arg ' , 'Another arg ' , a="b", key="somevalue")
except  Exception as e: 
  print( 'call generated unknown error --> ',repr(e))

print() # ========

hl_2.set_obj_for_strategy ( "do_something", Strategy_Referencing_Parent( hl_2 ))


o = hl_2.get_obj_for_strategy('do_something')

print(' (Low Level, iteration over method names): \n') # ========

if o:
    for method_attribute in 'anything', 'nothing':
        print (" --> trying bound method: ", method_attribute  )
        m = getattr(o, method_attribute, None)
        if m:
            tuple_of_args = ( 'one', )
            tuple_of_args += ( 'two',  'three' )
            kw = { 'my_method_name' : method_attribute }
            m( *tuple_of_args, **kw )
        else:
            print ('method not known')

print('\n (High Level, call a method that is known to exist): \n') # ========

if o:
    getobj = o.anything( "return", "referenced object ->" )
else:
    getobj = None

print('strategy object references parent:', getobj == hl_2, "\n")


#===    END      ===== MAIN function =====================
